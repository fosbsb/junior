<?php
error_reporting(E_ALL & ~E_NOTICE);
#Recuperando as váriaveis 

$nome = $_POST["nome"];
$questao_1 = $_POST["questao_1"];
$questao_2 = $_POST["questao_2"];
$questao_3 = $_POST["questao_3"];
$questao_4 = $_POST["questao_4"];
$questao_5 = $_POST["questao_5"];

# Verificando as resposta
$acertos = 0;

if ($questao_1 == "echo")
    $acertos++;
if ($questao_2 == "$")
    $acertos++;
if ($questao_3 == "function")
    $acertos++;
if ($questao_4 == "if")
    $acertos++;
if ($questao_5 == "POST")
    $acertos++;

# Exibindo respostas
?>
<center>

<?php if ($acertos == 5){ ?>
    <h1>Parabéns <?php echo $nome?>! Acertou TODAS as Questões.
    <img src="img/acertou.jpg" width="100">
<?php }elseif($acertos == 0){ ?>
    <h1>Que pena <?php echo $nome?>! Errou TODAS as Questões.
    <img src="img/errou.jpg" width="100">
<?php }else{ ?>
    <h1><?php echo $nome?>, você acertou <?php echo $acertos?>/5 questões!
    <img src="img/quase.png" width="100">
<?php } ?>
<p>
    <font size="2">
        <a href="formulario.php">Responder Novamente!</a>
    </font>
</p>
</center>