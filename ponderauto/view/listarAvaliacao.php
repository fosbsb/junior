<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Almendra Display' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Butcherman' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Mr De Haviland' rel='stylesheet'>
    </head>
    <body>
        <?php
        require_once '../dao/AvaliacaoDAO.php';
        include '../js/funcaoData.php';

        $avaliacaoDAO = new AvaliacaoDAO();
        $usuarios = $avaliacaoDAO->getAllAvaliacao();

        echo "<table border='1' align='center'>";
        echo "<tr>";
        echo "  <th>ID Usuário</th>";
        echo "  <th>Usuário</th>";
        echo "  <th>Senha</th>";
        echo "  <th>Perfil</th>";
        echo "  <th>Excluir</th>";
        echo "  <th>Alterar</th>";
        echo "</tr>";


        foreach ($usuarios as $c) {
            echo "<tr>";
            echo "  <td>{$c["idusuario"]}</td>";
            echo "  <td>{$c["usuario"]}</td>";
            echo "  <td>{$c["senha"]}</td>";
            echo "  <td>{$c["perfil_idperfil"]}</td>";
            echo "  <td><a href='../controller/excluirUsuarioByIdController.php?id={$c["idusuario"]}'>Excluir</a></td>";
            echo "  <td><a href='formAlterarUsuario.php?id={$c["idusuario"]}'>Alterar</a></td>";
            echo "</tr>";
        }

        echo "</table>";
        ?>

    </body>
</html>

