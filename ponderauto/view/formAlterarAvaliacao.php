<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Almendra Display' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Butcherman' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Mr De Haviland' rel='stylesheet'>
    </head>
    <?php
    require_once '../dao/AvaliacaoDAO.php';
    include '../js/funcaoData.php';  
    
    $idavaliacao = $_GET["id"];
    $avaliacaoDAO = new AvaliacaoDAO();
    $usuario = $avaliacaoDAO->getAvaliacaoById($idavaliacao);
   
    ?>
    <body>
        <form action="../controller/alterarUsuarioController.php" method="post">
            <input type="hidden" name="idusuario" value="<?php echo $usuario["idusuario"]?>"/>
            <table>
                <tr>
                    <td>Usuário:</td>
                    <td><input type="text" value="<?php echo $usuario["usuario"] ?>" name="usuario" size="50"/></td>
                </tr>
                <tr>
                    <td>Senha:</td>
                    <td><input type="text" value="<?php echo $usuario["senha"]?>" name="senha"/></td>
                </tr>                
                <tr>
                    <td>Perfil:</td>
                    <td><input type="text" value="<?php echo $usuario["perfil_idperfil"]?>" name="perfil"/></td>
                </tr>                                                                              
                <tr>
                    <td>Sexo:</td>
                    <td>
                        <?php
                        if ($usuario["perfil_idperfil"] == "masc"){
                            echo "Administrador <input type='radio' checked name='perfil_idperfil' value='1'/>";
                            echo "Usuário <input type='radio' name='sexo' value='2'/>";
                        }else{
                            echo "Administrador <input type='radio' name='sexo' value='masc'/>";
                            echo "Usuário <input type='radio' checked name='sexo' value='fem'/>";
                        }
                        ?>
                        
                    </td>
                </tr>                                                                             
                <tr>                    
                    <td colspan="2">
                        <input type="submit" value="Alterar"/>
                    </td>
                </tr>                                                                
            </table>
        </form>       
    </body>
</html>
