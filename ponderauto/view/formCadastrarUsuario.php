<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Almendra Display' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Butcherman' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Mr De Haviland' rel='stylesheet'>
    </head>
    <body>
        <div class="container">
            <p class="bg-danger text-white">Cadastrar Usuário</p>
            <form action="../controller/cadastrarAvaliadorController.php" method="post">
                <table class="table table-bordered">
                    <tr>
                        <td>Usuário:</td>
                        <td><input type="text" name="usuario" size="50"/></td>
                    </tr>
                    <tr>
                        <td>Senha:</td>
                        <td><input type="password" name="senha"/></td>
                    </tr>                                                                                     
                    <tr>
                        <td>Perfil:</td>
                        <td>
                            <div class="radio">
                                <label><input type="radio" name="perfil" value="1"/>Administrador</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="perfil" value="2"/>Avaliador</label>
                            </div>                           
                        </td>
                    </tr>                                                                         
                    <tr>                    
                        <td colspan="2">
                            <input type="submit" value="Cadastrar" class="btn btn-info"/>
                        </td>
                    </tr>                                                                
                </table>
            </form>
            <center>
                <?php
                if (!empty($_GET["msg"])) {
                    echo $_GET["msg"];
                }
                ?>
            </center>
        </div>  
    </body>
</html>
