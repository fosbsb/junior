<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Almendra Display' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Butcherman' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Mr De Haviland' rel='stylesheet'>
    </head>
    <body>
        <div class="container">
            <p class="bg-danger text-white">Cadastrar Avaliação</p>
            <form action="../controller/cadastrarAvaliacaoController.php" method="post">
                <input type="text" value="<?php echo  $_SESSION["usuario"]?>" name="idusuario">
                <table class="table table-bordered">
                    <tr>
                        <td colspan="2"> Aparência externa</td>
                    </tr>
                    <tr>
                        <td> Pintura</td>
                        <td>
                            Queimada<input type="checkbox" name="pintura" value="1"/>
                           <!-- Opaca<input type="checkbox" name="pintura" value="2"/>
                            Desigual<input type="checkbox" name="pintura" value="3"/>
                            Manchada<input type="checkbox" name="pintura" value="4"/>-->
                        </td>
                    </tr>

                    <tr>                    
                        <td colspan="2">
                            <input type="submit" value="Cadastrar" class="btn btn-info"/>
                        </td>
                    </tr>                                                                
                </table>
            </form>
            <center>
                <?php
                if (!empty($_GET["msg"])) {
                    echo $_GET["msg"];
                }
                ?>
            </center>
        </div>  
    </body>
</html>
