<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" type="image/png" href="../imagens/Logo PonderAuto PNG.png" /><!--Usando faviconIcon na Aba do URL-->
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Almendra Display' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Butcherman' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Mr De Haviland' rel='stylesheet'>
    </head>
    <body>
        <?php
        session_start();
        include 'validaLogin.php';
        ?> 
        <table width="100%">
            <tr>
                <td width="85%">
                    <?php
                    switch ($_SESSION["perfil"]) {
                        case "Administrador":
                            include './menuAdministrador.php';
                            break;
                        case "Avaliador":
                            include './menuAvaliador.php';
                            break;
                    }
                    ?>
                    <br>
                </td>
                <td align="right" width="15%">
                    <?php
                    if (isset($_SESSION["usuario"])) {
                        echo "Usuário Logado: ", $_SESSION["usuario"];
                        echo "<br>";
                        echo "Perfil: ", $_SESSION["perfil"];
                    }
                    ?>
                    <br>
                    <a href="../controller/logoffController.php">sair</a>
                </td>
            </tr>
        </table>
        <table id="tablehome">
            <tr>         
                <td>
                    <iframe name="centro" src="" width="100%" height="100%" frameborder="0"></iframe>
                </td>
            </tr>                
        </table>
        <?php include '../footer.php'; ?>
    </body>
</html>
