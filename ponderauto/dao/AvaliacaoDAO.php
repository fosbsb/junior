<?php

require_once 'conexao/conexao.php';

class AvaliacaoDAO {

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }

    public function getAllAvaliacao() {
        try {
            $sql = "SELECT * FROM usuario";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $avaliacoes = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $avaliacoes;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function salvarAvaliacao(AvaliacaoDTO $AvaliacaoDTO) {
        try {
            $sql = "INSERT INTO avalicao (idusuario,pintura) 
                    VALUES (?,?)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $AvaliacaoDTO->getIdusuario());
            $stmt->bindValue(2, $AvaliacaoDTO->getPintura());
          
            return $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function excluirAvaliacao($idavaliacao) {
        try {
            $sql = "DELETE FROM avaliacao 
                   WHERE idavaliacao = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idavaliacao);
            $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function getAvaliacaoById($idavaliacao) {
        try {
            $sql = "SELECT * FROM usuario WHERE idusuario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idavaliacao);
            $stmt->execute();
            $avaliacao = $stmt->fetch(PDO::FETCH_ASSOC);
            return $avaliacao;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function updateAvaliacaoById(AvaliacaoDTO $AvaliacaoDTO) {
        try {
            $sql = "UPDATE avaliacao SET avaliacao=?,
                                       senha=?,
                                       perfil_idperfil=?
                    WHERE idavaliacao= ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $avaliacaoDTO->getUsuario());
            $stmt->bindValue(2, $avaliacaoDTO->getSenha());
            $stmt->bindValue(3, $avaliacaoDTO->getPerfil_idperfil());
            $stmt->bindValue(4, $avaliacaoDTO->getIdusuario());
            $stmt->execute();
            
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

}

?>
