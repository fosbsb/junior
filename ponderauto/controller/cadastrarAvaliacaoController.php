<?php
require_once '../dto/AvaliacaoDTO.php';
require_once '../dao/AvaliacaoDAO.php';


// recuperei os dados do formulario
$idusuario = $_POST["idusuario"];
$pintura = $_POST["pintura"];



$avaliacaoDTO = new AvaliacaoDTO;
$avaliacaoDTO->setIdusuario($idusuario);
$avaliacaoDTO->setPintura($pintura);

$avaliacaoDAO = new AvaliacaoDAO();
$sucesso = $avaliacaoDAO->salvarAvaliacao($avaliacaoDTO);

if ($sucesso){
   $msg = "Cadastrado com sucesso"; 
   echo "<script>";
   echo "window.location.href = '../view/formCadastrarAvaliacao.php?msg={$msg}';";
   echo "</script> ";
}
?>

