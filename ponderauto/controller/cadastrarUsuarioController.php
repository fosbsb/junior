<?php
require_once '../dto/UsuarioDTO.php';
require_once '../dao/UsuarioDAO.php';


// recuperei os dados do formulario
$usuario = $_POST["usuario"];
$senha = md5($_POST["senha"]);
$perfil = $_POST["perfil"];


$usuarioDTO = new UsuarioDTO();
$usuarioDTO->setUsuario($usuario);
$usuarioDTO->setSenha($senha);
$usuarioDTO->setPerfil_idperfil($perfil);

$usuarioDAO = new UsuarioDAO();
$sucesso = $usuarioDAO->salvarUsuario($usuarioDTO);

if ($sucesso){
   $msg = "Cadastrado com sucesso"; 
   echo "<script>";
   echo "window.location.href = '../view/formCadastrarUsuario.php?msg={$msg}';";
   echo "</script> ";
}
?>

