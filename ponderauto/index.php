<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" type="image/png" href="imagens/Logo PonderAuto PNG.png" /><!--Usando faviconIcon na Aba do URL-->
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <title>Login</title>
    </head>
    <body style="background-color: black">
    <center  style="margin-top: 170px;">
        <button onclick="document.getElementById('id01').style.display = 'block'" style="background-color: black; border:  none;">   
            <img src="imagens/Logo para fundo preto.png" alt="Logo" />
        </button>
    </center>
    <div class="w3-container">
        <div id="id01" class="w3-modal w3-black">
            <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

                <div class="w3-center"><br>
                    <span onclick="document.getElementById('id01').style.display = 'none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
                    <img src="imagens/Logo PonderAuto PNG.png" alt="Avatar" style="width:45%" class="w3-margin-top">
                </div>
                <form class="w3-container" action="controller/loginController.php" method="post">
                    <div class="w3-section">

                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Usuário" name="usuario" required style="color: black;">

                        <input class="w3-input w3-border" type="password" placeholder="Senha" name="senha" required style="color: black;">
                        <button class="w3-button w3-block w3-black w3-section w3-padding" type="submit">Login</button>
                    </div>
                </form>

                <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                    <button onclick="document.getElementById('id01').style.display = 'none'" type="button" class="w3-button w3-red">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <center>
        <?php
        if (!empty($_GET["msg"])) {
            echo "<div class='w3-panel w3-red' style='width:300px;'>" . ""."<h3>Atenção!</h3>" . "<p>" . $_GET["msg"] . "</p> " . "</div>";
        }
        ?>
    </center>
    <?php
    include './footer.php';
    ?>
</body>
</html>

